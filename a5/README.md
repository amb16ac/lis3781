
# Advanced Database Management

## Aaron Baughman

### Assignment 5 Requirements:

1. Tables and Data in MS SQL Server Development
2. SQL Solutions

#### Business Rules:

- Business Rules:Expanding upon the high-volumehome office supply company’s data tracking requirements, the CFO requests your services again to extend the data model’s functionality. The CFO has read about the capabilities of data warehousing analytics and business intelligence (BI), and is looking to develop a smaller data mart as a test platform. He is under pressure from the members of the company’s board of directors who want to reviewmore detailed salesreports based upon the following measurements:
- Product
- Customer
- Sales representative
- Time(year, quarter, month, week, day, time)
- Location
Furthermore, the board members want location to be expanded to include the following characteristics of location:
- Region
 - State
 - City
 - Store

##### README.md file should include the following items:

* Screenshot of ERD


#### Assignment Screenshots:

*Screenshot of Populated Tables*:

![ERD Screenshot](imgs/A5ERD.PNG)
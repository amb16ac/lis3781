
# LIS 3781

## Aaron Baughman

### Assignment 2 Requirements:

1. Use SQL to create database locally hosted
2. Populate tables with data
3. Obfuscate Personal Information
4. Create users & set Privileges

#### README.md file should include the following items:

* Screenshot of completed code
* Screenshot of populted tables


#### Assignment Screenshots:

*Screenshot of complete code part 1*:

![Code Screenshot](imgs/code1.png)

*Screenshot of complete code part 2*:

![Code Screenshot](imgs/code2.png)

*Screenshot of populated tables*:

![Populated Table Screenshot](imgs/tables.png)

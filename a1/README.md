
# LIS3781 Advanced Database Management

## Aaron Baughman

### Assignment 1 Requirements:

1. git Commands with a Short Description
2. Assignment Screenshots
3. Bitbucket repo links

> #### Git commands w/short descriptions:

1. git init - creates new git repo
2. git status - shows state of current directory & staging area
3. git add - adds change made to the staging area
4. git commit - saves changes in local repo
5. git push - pushes changes in local repo to remote repo
6. git pull - pulls changes in remote repo to local repo
7. git branch - lists all branches in repo

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of A1 ERD*:

![A1 Entity Relationship Diagram](img/A1ERD.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/amb16ac/bitbucketstationlocations/ "Bitbucket Station Locations")


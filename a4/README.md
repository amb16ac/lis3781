
# Advanced Database Management

## Aaron Baughman

### Assignment 4 Requirements:

1. Tables and Data in MS SQL Server Development
2. SQL Solutions

#### Business Rules:

- A sales representative has at least one customer, and each customer has at leastone sales rep on any given day(as it is a high-volume organization).
- A customer places at least one order. However, each order is placed by only one customer.
- Each order contains at least one order line. Conversely, each order line is contained in exactly one order.
- Each product may be on a number of order lines. Though, each order line contains exactly one product id (though, each product id may have a quantity of more than one included, e.g., “oln_qty”).
- Each order is billed on one invoice, and each invoice is a bill for exactly one order(by only one customer).
- An invoice can have one (full),or can have many payments (partial). Though, each payment is made to only one invoice.
- A store hasmany invoices, but each invoice is associated with only one store.
- A vendor providesmany products, but each product is provided by only one vendor.
- Must track yearly history of sales reps, including(also, see Entity-specific attributes below): yearly sales goal, yearly total sales, yearly total commission (in dollars and cents).
- Must track history of products, including: cost, price, and discount percentage (if any).

##### README.md file should include the following items:

* Screenshot of ERD


#### Assignment Screenshots:

*Screenshot of Populated Tables*:

![ERD Screenshot](imgs/A4ERD.png)
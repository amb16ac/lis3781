
# Advanced Database Management

## Aaron Baughman

### Project 1 Requirements:

*Business Rules:*

- An attorney is retained by (or assigned to) one or more clients for each case.
- A client has (or is assigned to) one or more attorneys for each case.
- An attorney has one or more cases.
- A client has one or more cases.
- Each court has one or more judges adjudicating.
- Each judge adjudicates upon exactly one court.
- Each judge may preside over more than one case.
- Each case that goes to court is presided over by exactly one judge.
- A person can have more than one phone number.

#### README.md file should include the following items:

* Screenshot of Completed ERD


#### Assignment Screenshots:

*Screenshot of Completed ERD*:

![ERD Screenshot](img/P1ERD.png)

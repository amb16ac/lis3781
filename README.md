
# LIS3781 Advanced Database Management

## Aaron Baughman

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](/a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Interpret Business Rules to develop a functioning ERD

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Setup AMPPS
    - Forward Engineer Database
    - Setup Users and Grant Database Permissions

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Develop Database in Oracle SQL Developer
    - Forward Engineer Database

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Develop Database using MySQL
    - Create ERD with Cardinalities and Layering
    - Salt and Hash Unique SSN to obfuscate personal data in tables

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Develop Database in MS SQL Developer
    - Salt and Hash Unique SSN 

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Develop Database in MS SQL Developer
    - Salt and Hash Unique SSN 
    - Include Detailed Reports

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Install Homebrew and MongoDB
    - Using NoSQL, run basic commands and reports


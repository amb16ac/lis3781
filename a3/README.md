
# Advanced Database Management

## Aaron Baughman

### Assignment 3 Requirements:

1. Tables and Data in Oracle SQL Development
2. SQL Solutions

#### README.md file should include the following items:

* Screenshot of SQL Code
* Screenshot of Populated Tables in Oracle Environment


#### Assignment Screenshots:

*Screenshot of SQL Code*:

![SQL Code Screenshot](imgs/code.png)

*Screenshot of Populated Tables*:

![Populated Tables Screenshot](imgs/tables.png)